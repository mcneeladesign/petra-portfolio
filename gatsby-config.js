module.exports = {
  siteMetadata: {
    title: "Petra Venturini portfolio",
    author: "Petra Venturini",
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/pages`,
        name: "pages",
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 2048,
              wrapperStyle: `margin-bottom: 1.0725rem`,
              linkImagesToOriginal: false,
              backgroundColor: '#ffffff',
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          "gatsby-remark-prismjs",
          "gatsby-remark-copy-linked-files",
          "gatsby-remark-smartypants",
        ],
      },
    },
    {
    resolve: `gatsby-plugin-manifest`,
    options: {
      name: "Petra Venturini",
      short_name: "Petra",
      start_url: "/",
      background_color: "#ffffff",
      theme_color: "#ffffff",
      display: "standalone",
      icon: "src/icons/icon.png", // This path is relative to the root of the site.
      icons: [
          {
            src: `/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    //{
      //resolve: `gatsby-plugin-google-analytics`,
      //options: {
        //trackingId: `ADD YOUR TRACKING ID HERE`,
      //},
    //},
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Oswald:400`,
          `Enriqueta:400` // you can also specify font weights and styles
        ]
      }
    }
  ],
}
