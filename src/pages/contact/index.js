import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import Menu from '../../components/Menu'
import Logo from '../../components/Logo'

import BaseLayout from '../../components/BaseLayout'
// import Menu from '../../components/Menu'

import rightArrow from './right-arrow.svg'
import './index.scss'

function encode(data) {
  return Object.keys(data)
      .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
      .join("&");
}

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  }

  handleSubmit = e => {
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({ "form-name": "contact-petra", ...this.state })
    })
      .then(() => {
        // alert("Success!");
        window.location.assign('/thanks');
      })
      .catch((error) => {
        console.log(error);
        window.location.assign('/error');
      });
    e.preventDefault();
  };


  render() {
    return (
      <div className="Contact">
        <div className="Header">
          <Logo />
          <Menu />
        </div>
        <Helmet title={get(this, 'props.data.site.siteMetadata.title')} />
        <BaseLayout title="Contact">
          <form
            name="contact-petra"
            method="post"
            action="/thanks/"
            data-netlify="true"
            data-netlify-honeypot="bot-field"
            onSubmit={this.handleSubmit}
          >
            <p hidden style={{display: 'none'}}>
              <label>
                Don’t fill this out: <input name="bot-field" />
              </label>
            </p>
              <input placeholder="what's your name..." type="text" name="name" required onChange={this.handleChange}/>
              <input placeholder="...and your email" type="email" name="email" required onChange={this.handleChange}/>
              <textarea placeholder="don't be shy and say hi" name="message" onChange={this.handleChange}/>
              <div>
                <button type="submit">Send <img src={rightArrow} alt=""/></button>
              </div>
          </form>
        </BaseLayout>
      </div>
    )

  }
}

Contact.propTypes = {
  route: React.PropTypes.object,
}

export default Contact

export const pageQuery = graphql`
  query Contact {
    site {
      siteMetadata {
        title
      }
    }
  }
`
