---
title: Float
subtitle: Packaging
date: "2018-11-04T22:12:03.284Z"
path: "/float/"
featuredImage: "./float_intro_new.png"
student: true 
---

<section class="hero-section">
  <div class="hero-text">
    <div class="hero-text-titles">
      <div class="hero-text-content">
        <h2>Float</h2>
        <p>Biodegradable balloon packaging, that is aimed at a specified demographic.</p>
        <p>Giving a sophisticated and intriguing look to the package, with a nurturing colour palette and a variety of patterns showcasing the transformation between a solid and lighter state</p>
        <ul>
          <h5>PROJECT INVOLVEMENT</h5>
          <li>- Research</li>
          <li>- Brand messaging</li>
          <li>- Brand identity</li>
          <li>- Naming</li>
          <li>- Logo development</li>
          <li>- Packaging design</li>
        </ul>
        <ul>
          <h5>TOOLKIT</h5>
          <li>- InDesign</li>
          <li>- Illustrator</li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="main-section">
  <img src="./1-forweb_float_logo.gif" alt="" />
  <img src="./2-forweb_mockup.png" alt="" />
  <img src="./3-forweb_3pack.png" alt="" />
</section>
