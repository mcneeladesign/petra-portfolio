---
title: Das Food
subtitle: Digital design - UI/UX
date: "2018-07-04T22:12:03.284Z"
path: "/das-food-ordering-app/"
featuredImage: "./dasfood_intro.png"
student: true 
---

<section class="hero-section">
  <div class="hero-text">
    <div class="hero-text-titles">
      <div class="hero-text-content">
        <h2>Das food</h2>
        <p>A food ordering app for an online German restaurant.</p>
        <p>Inspired by Berlins unique artistic feel to create a user interface that is approachable yet dynamic, whilst promoting a great user experience.</p>
        <ul>
          <h5>PROJECT INVOLVEMENT</h5>
          <li>- Concept development</li>
          <li>- Brand identity</li>
          <li>- Tone of voice</li>
          <li>- Naming</li>
          <li>- Logo creation</li>
          <li>- Illustration of icon set</li>
          <li>- UI & UX</li>
        </ul>
        <ul>
          <h5>TOOLKIT</h5>
          <li>- Sketch</li>
          <li>- Illustrator</li>
          <li>- InVision</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="hero-text hero-img">
    <img src="./1-forwe_sketch.png" alt="" />
  </div>
</section>

<section class="main-section">
  <div class="hero-img">
    <img src="./1-forwe_sketch.png" alt="" />
  </div>
  <img src="./2-forweb_holding.png" alt="" />
  <img src="./forweb_styletile.png" alt="" />
  <img src="./3-forweb_screens.png" alt="" />
  <img src="./4-forweb_vespa.png" alt="" />
</section>
