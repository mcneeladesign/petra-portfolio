---
title: Power house museum
subtitle: Branding
date: "2018-12-04T22:12:03.284Z"
path: "/power-houes-museum/"
featuredImage: "./museum_intro.png"
student: true 
---

<section class="hero-section">
  <div class="hero-text">
    <div class="hero-text-titles">
      <div class="hero-text-content">
        <h2>POWER HOUSE MUSEUM</h2>
        <p>Re-branding of The Cup Noodle museum in Osaka, Japan with a goal to attract a new demographic and to create excitement.</p>
        <p>The new look is focused around the determination of Momofuku Ando, the creator of Ramen Noodles. Strong/bold type with a neutral colour palette that has an element of surprise with strong contrast. Flyer series created around the 3 inventions in an intriguing tone of voice to raise interest in the museum, following the strong brand guidelines throughout the rollout.</p>
        <ul>
          <h5>PROJECT INVOLVEMENT</h5>
          <li>- Research</li>
          <li>- Concept development</li>
          <li>- Naming</li>
          <li>- Logo design</li>
          <li>- Brand identity</li>
          <li>- Brand messaging</li>
          <li>- Collateral rollout</li>
        </ul>
        <ul>
          <h5>TOOLKIT</h5>
          <li>- Sketch</li>
          <li>- InVision</li>
          <li>- Illustrator</li>
          <li>- Photoshop</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="hero-text hero-img">
    <img src="./forweb_sketch_sml.png" alt="" />
  </div>
</section>

<section class="main-section">
  <div class="hidden">
    <img src="./new_logo_video_forweb.gif" alt="" />
  </div>
  <div class="hero-img">
    <img src="./forweb_sketch_sml.png" alt="" />
  </div>
  <img src="./new_logo_video_forweb.gif" alt="" />
  <img src="./forweb_05.png" alt="" />
  <div class="show-small">
    <img src="./forweb_web_sml.png" alt="" />
  </div>
  <div class="show-large">
    <img src="./forweb_web.png" alt="" />
  </div>
  <div class="show-small">
    <img src="./forweb_03_sml.png" alt="" />
  </div>
  <div class="show-large">
    <img src="./forweb_03.png" alt="" />
  </div>
  <!-- <img src="./forweb_noodlebox.png" alt="" /> -->
</section>
