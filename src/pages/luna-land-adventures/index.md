---
title: Luna land adventures
subtitle: Editorial
date: "2018-08-04T22:12:03.284Z"
path: "/luna-land-adventures/"
featuredImage: "./lunaland_intro.png"
student: true 
---

<section class="hero-section">
  <div class="hero-text">
    <div class="hero-text-titles">
      <div class="hero-text-content">
        <h2>Luna land adventures</h2>
        <p>Luna Land Adventurers is a corporate report for an amusement park, that needed to be presented in an exciting way.</p>
        <p>Creating movement on the pages with energetic, escaping illustrations to capture the passion and essence of the business. Using a complimentary colour palette that brings across the diverse range of emotions when at an amusement park.</p>
        <ul>
          <h5>PROJECT INVOLVEMENT</h5>
          <li>- Idea generation</li>
          <li>- Concept development</li>
          <li>- Type setting</li>
          <li>- Type illustration</li>
        </ul>
        <ul>
          <h5>TOOLKIT</h5>
          <li>- Illustrator</li>
          <li>- InDesign</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="hero-text hero-img">
    <img src="./1-forweb_sketch.png" alt="" />
  </div>
</section>

<section class="main-section">
  <div class="hero-img">
    <img src="./1-forweb_sketch.png" alt="" />
  </div>
  <img src="./2-forweb_frontandtimeline.png" alt="" />
  <div class="show-small">
    <img src="./3-Photorealistic-Magazine-MockUp_sml.png" alt="" />
  </div>
  <div class="show-large">
    <img src="./3-Photorealistic-Magazine-MockUp.png" alt="" />
  </div>
  <div class="show-small">
    <img src="./4-forweb_infographic_sml.png" alt="" />
  </div>
  <div class="show-large">
    <img src="./4-forweb_infographic.png" alt="" />
  </div>
</section>
