---
title: The silver linings playbook
subtitle: Book cover (handmade)
date: "2018-09-04T22:12:03.284Z"
path: "/the-silver-linings-playbook/"
featuredImage: "./handmade_intro_image.png"
student: true 
---

<section class="hero-section">
  <div class="hero-text">
    <div class="hero-text-titles">
      <div class="hero-text-content">
        <h2>The silver linings playbook</h2>
        <p>To design a book cover with a handmade approach.</p>
        <p>Created an origami form that represents the fragile yet heavy state of mental illnesses such as bipolar-disorder, to highlight the theme of the book.</p>
        <ul>
          <h5>PROJECT INVOLVEMENT</h5>
          <li>- Research</li>
          <li>- Concept development</li>
          <li>- Creating origami</li>
          <li>- Art directing photo shoot</li>
        </ul>
        <ul>
          <h5>TOOLKIT</h5>
          <li>- Photoshop</li>
          <li>- InDesign</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="hero-text hero-img">
    <img src="./1-handmade_process.png" alt="" />
  </div>
</section>

<section class="main-section">
  <div class="hero-img">
    <img src="./1-handmade_process.png" alt="" />
  </div>
  <img src="./2-forweb_books.png" alt="" />
  <img src="./3-forweb_stacked.png" alt="" />
  <img src="./4-forweb_holding.png" alt="" />
</section>
