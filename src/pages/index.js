import React from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import Menu from '../components/Menu'
import Logo from '../components/Logo'

import './index.scss'

class PortfolioIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    const posts = get(this, 'props.data.allMarkdownRemark.edges')

    return (
      <div className="Portfolio">
        <div className="Header">
          <Logo />
          <Menu />
        </div>
        <Helmet title={get(this, 'props.data.site.siteMetadata.title')} />
        {posts.map(post => {
          if (post.node.path !== '/404/') {
            const title = get(post, 'node.frontmatter.title') || post.node.path
            return (
              <div key={post.node.frontmatter.path}>
                <Link to={post.node.frontmatter.path} >
                  <div className="overlay">
                    <h4>{post.node.frontmatter.title}</h4>
                    <p>{post.node.frontmatter.subtitle}</p>
                  </div>
                  <Img sizes={post.node.frontmatter.featuredImage.childImageSharp.sizes} />
                </Link>
                <p dangerouslySetInnerHTML={{ __html: post.node.excerpt }} />
              </div>
            )
          }
        })}
      </div>
    )
  }
}

PortfolioIndex.propTypes = {
  route: React.PropTypes.object,
}

export default PortfolioIndex

export const pageQuery = graphql`
  query PortfolioQuery {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          frontmatter {
            path
            date(formatString: "DD MMMM, YYYY")
            title
            subtitle
            featuredImage {
                childImageSharp{
                    sizes(maxWidth: 1000) {
                        ...GatsbyImageSharpSizes
                    }
                }
            }
          }
        }
      }
    }
  }
`
