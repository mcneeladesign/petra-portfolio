---
title: The tooth gang
subtitle: Campaign
date: "2018-10-04T22:12:03.284Z"
path: "/the-tooth-gang/"
featuredImage: "./campaign_intro.png"
student: true 
---

<section class="hero-section">
  <div class="hero-text">
    <div class="hero-text-titles">
      <div class="hero-text-content">
        <h2>The tooth gang</h2>
        <p>The Tooth-Gang is an awareness campaign aimed at educating young families on the importance of oral hygiene.</p>
        <p>The campaign is based around an event that is presented in 3 stages. The first stage is creating a fun and friendly tone of voice to generate excitement with a series of teaser posters. During the event families receive ToothGang branded collateral and after the event an iPad game is released to help reaffirm the educational message for the children.</p>
        <ul>
          <h5>PROJECT INVOLVEMENT</h5>
          <li>- Research</li>
          <li>- Concept development</li>
          <li>- Brand identity</li>
          <li>- Tone of voice</li>
          <li>- Character illustration</li>
          <li>- Collateral rollout</li>
          <li>- Digital application</li>
        </ul>
        <ul>
          <h5>TOOLKIT</h5>
          <li>- InDesign</li>
          <li>- Illustrator</li>
          <li>- Photoshop</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="hero-text hero-img">
    <img src="./1-forweb_sketch-1.png" alt="" />
  </div>
</section>

<section class="main-section">
  <div class="hidden">
    <img src="./5-forweb_tshirt_small.gif" alt="" />
    <img src="./5-forweb_tshirt.gif" alt="" />
  </div>
  <div class="hero-img">
    <img src="./1-forweb_sketch-1.png" alt="" />
  </div>
  <img src="./forweb_poster_01_2.png" alt="" />
  <img src="./forweb_poster_02_2.png" alt="" />
  <div class="show-large">
    <img src="./4-forweb_totebag.png" alt="" />
  </div>
  <div class="show-small">
    <img src="./4-forweb_totebag_sml.png" alt="" />
  </div>
  <div class="show-large">
    <img src="./5-forweb_tshirt.gif" alt="" />
  </div>
  <div class="show-small">
    <img src="./5-forweb_tshirt_small.gif" alt="" />
  </div>
  <img src="./6-forweb_ipad_kid.png" alt="" />
  <img src="./7-forweb_ipad_screens.png" alt="" />
</section>
