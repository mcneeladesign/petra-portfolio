---
title: ACCENTURE IN-HOUSE SIGNAGES
subtitle: Signage
student: false 
date: "2018-08-04T22:12:03.284Z"
path: "/accenture/"
featuredImage: "./intro.png"
---

<section class="hero-section">
  <div class="hero-text">
    <div class="hero-text-titles">
      <div class="hero-text-content">
        <h2>ACCENTURE IN-HOUSE SIGNAGES</h2>
        <p>Accenture is a global management consulting and professional services firm. I had the opportunity to design a variety of in house signages for their brand new office in Barangaroo, Australia. All of the signages needed to be on-brand, using their main graphical element and meeting the specifications outlined in their latest brand guidelines.</p>
        <ul>
          <h5>PROJECT INVOLVEMENT</h5>
          <li>- Research</li>
          <li>- Assessment of brand<br/>guidelines provided</li>
        </ul>
        <ul>
          <h5>TOOLKIT</h5>
          <li>- Illustrator</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="hero-text hero-img">
    <!-- <img src="./1-forweb_sketch.png" alt="" /> -->
  </div>
</section>
  <img src="./project-01.png" alt="" />
  <img src="./project-02.png" alt="" />
  <img src="./project-03.png" alt="" />
</section>
