import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import Menu from '../../components/Menu'
import Logo from '../../components/Logo'

import BaseLayout from '../../components/BaseLayout'
import './index.scss'

class HomeIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    const posts = get(this, 'props.data.allMarkdownRemark.edges')

    return (
      <div>
        <div className="Header">
          <Logo />
          <Menu />
        </div>
        <Helmet title={get(this, 'props.data.site.siteMetadata.title')} />
        <BaseLayout title="Hi">
          <div className="About">
            <p>I'm Petra. I love to create concepts that can deliver a message and make a difference. Whether it is within a small business or a personal project of mine or others. Design is powerful and challenging, that's what I like about it. I have a diverse style and keen eye for detail. If you have a project in mind to talk about or just have a few questions, dont be shy and say <Link to="/contact">hi</Link>. </p>
          </div>
        </BaseLayout>
      </div>
    )
  }
}

HomeIndex.propTypes = {
  route: React.PropTypes.object,
}

export default HomeIndex

export const pageQuery = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
