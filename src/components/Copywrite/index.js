import React from 'react'

import './index.scss'

class Menu extends React.Component {
  render() {
    return (
      <div className="Copywrite">
        <p>Copyright of Petra Venturini{this.props.isStudent ? ' | Student brief' : ''}</p>
      </div>
    )
  }
}

export default Menu
