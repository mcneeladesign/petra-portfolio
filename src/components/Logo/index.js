import React from 'react'
import Link from 'gatsby-link'

import LogoSvg from './petra_logo-2.svg'
import LogoSvgShort from './logo.svg'
import './index.scss'

class Logo extends React.Component {
  render() {
    return (
      <div className="Logo">
        <Link to={'/'} >
          <img className="largeLogo" src={LogoSvg} alt="PV"/>
          <img className="smallLogo" src={LogoSvgShort} alt="PV"/>
        </Link>
      </div>
    )
  }
}

export default Logo
