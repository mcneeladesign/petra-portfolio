import React from 'react'
import Helmet from 'react-helmet'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Copywrite from '../components/Copywrite'
import './index.scss';

class BlogPostTemplate extends React.Component {
  goBack() {
    window.history.back();
  }
  render() {
    const post = this.props.data.markdownRemark
    const siteTitle = get(this.props, 'data.site.siteMetadata.title')
    return (
      <div className="postListItem">
        <div className="closeBtn">
          <button onClick={() => this.goBack()}>
            <span></span>
            <span></span>
            <span></span>
          </button>
        </div>
        <Helmet title={`${post.frontmatter.title} | ${siteTitle}`} />
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
        <Copywrite isStudent={post.frontmatter.student} />
      </div>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      id
      html
      frontmatter {
        title
        subtitle
        student
      }
    }
  }
`
